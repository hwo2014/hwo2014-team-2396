import json
import socket
import sys
import math


def r2d(v):
    return (180.0*v) / math.pi

def d2r(v):
    return (math.pi * v) / 180.0

lstr = ""
def log(str, send = True):
    global lstr
    if str is not None:
        lstr += str
    if send:
        if len(lstr) > 0:
            pass #print lstr
        lstr = ""

class Piece:

    def __init__(self, pid, data, prev):

        self.slid = 0
        self.id = pid
        self.prev = prev
        if prev is not None:
            prev.set_next(self)

        if data.has_key('length'):
            self.length = float(data['length'])
            self.radius = 0
            self.angle = 0
        else:
            self.radius = float(data['radius'])
            self.angle = float(data['angle'])
            self.length = (self.radius * math.pi * abs(self.angle)) / 180.0

        self.switch = False
        if data.has_key('switch'):
            self.switch = True

        self.cached_speed = {}

        # the max speed we should enter with 
    def max_speed(self, car, stopat = None):

        # tune

        min = 5

        if stopat == self:
            if self.angle == 0:
                return 100
            else:
                return min

        ofs = 0
        if car.pos is not None:
            ofs = car.pos.offset

        ck = str(ofs) + "_" + str(car.angle)

        if stopat is None:
            stopat = self
        elif self.cached_speed.has_key(ck) and car.pos is not None and car.pos.piece.id != self.id and self.slid == car.slid_k:
            return self.cached_speed[ck]

        if self.angle == 0:
            ns = self.next.max_speed(car, stopat)
            return ns + car.brake_speed(self.length, ns)

        max = self.next.max_speed(car, stopat)
        #max += car.brake_speed(self.length, max)

        nocache = False
        increase = 0.8 #1.0 #1.6
        max_f = 4.5 #0.5 #1.05 #5.6 #1.02
        max_f = max_f * ((60 - abs(car.angle)) / 60.0)

        rad = self.radius
        if car.pos is not None:
            totlen = self.len(car.pos.offset)

            if car.pos.piece.id == self.id:
                #rad += (increase * rad * (car.pos.dist/totlen))
                totlen -= increase*car.pos.dist
                nocache = True

            if totlen < 0:
                totlen = 0

        else:
            totlen = self.len(0)


        safe_speed = math.sqrt(self.radius * car.slid_k)
        safe_force = (car.mass() * (safe_speed**2)) / self.radius
        while min < max:
            
            force = (car.mass() * (min**2)) / rad
            if force < safe_force:
                totf = 0
            else:
                of = force-safe_force
                totf = abs((force * self.angle) / 360.0)

                totf = abs((of * self.angle) / 360.0)

                totf = abs((of * totlen)/100)

            if totf < max_f:
                min += 0.01
            else:
                min -= 0.01
                break

        if not nocache:
            #if self.cached_speed.has_key(ofs) and self.cached_speed[ofs] != min:
            #    print "adjusting speed for %d/%d: %f => %f" % (self.id, ofs, self.cached_speed[ofs], min)
            self.cached_speed[ck] = min
            self.slid = car.slid_k
        return min


    def len(self, offset):

        if self.angle == 0:
            return self.length
        
        if (self.angle < 0):
            offset = -offset
        
        length = ((self.radius - offset) * math.pi * abs(self.angle)) / 180.0
        #length = ((self.radius) * math.pi * abs(self.angle)) / 180.0
        return length

    def set_next(self, p):
        self.next = p

class Lane:

    def __init__(self, index, data):
        self.offset = float(data['distanceFromCenter'])
        self.index = index

class Track:

    def __init__(self, track):

        self.pieces = []
        prev, first = None, None
        for p in track['pieces']:
            pc = Piece(len(self.pieces), p, prev)
            self.pieces.append(pc)

            if first is None:
                first = pc
            prev = pc
        first.prev = prev
        prev.set_next(first)

        self.lanes = {}
        for l in track['lanes']:
            self.lanes[int(l['index'])] = Lane(int(l['index']), l)
            
        self.id = track['id']
        self.name = track['name']

        start = track['startingPoint']
        self.start = [ float(start['position']['x']), float(start['position']['y']), float(start['angle']) ]
        

class CarPos:

    def __init__(self, p, track):
        self.piece = track.pieces[int(p['pieceIndex'])]
        self.dist = float(p['inPieceDistance'])
        self.lane = int(p['lane']['startLaneIndex'])
        self.laneto = int(p['lane']['endLaneIndex'])
        self.lap = int(p['lap'])
        self.offset = track.lanes[self.lane].offset
        self.angle = 0

time_unit = 1.0

class Turbo:

    def __init__(self, data):
        self.ticks = data['turboDurationTicks']
        self.factor = data['turboFactor']
        self.ms = data['turboDurationMilliseconds']

class Car:

    def __init__(self, data):
        self.id = cid(data)
        self.color = data['color']
        self.name = data['name']
        self.track = 0

        self.pos = None
        self.lastpos = None
        self.speed = 0
        self.angle = 0
        self.force = 0
        self.last_speed = 0
        self.throttle = -1
                    
        self.switching = -1;
        self.turbo = None

        self.max_force = 0
        self.max_angle = 0
        
        self.drag_k = 0.020408 #0.1
        self.acc_k = 0
        self.speed_bump = False
        self.slid_k = 0.22

    def check_constants(self):

        td = self.speed - self.last_speed
        if self.throttle == 0:
        
            # calculate drag, moving average
            if td < 0 and not self.speed_bump and self.speed != 0:
                ndk = (self.last_speed - self.speed) / self.speed
                self.drag_k = ndk
                """
                if self.drag_k == 0 or True:
                    self.drag_k = ndk
                else:
                    self.drag_k = (self.drag_k*0.5) + (ndk*0.5)
                """
                #print "drag k is %f (from %f)" % (self.drag_k, ndk)

        elif self.throttle > 0:

            if self.pos.piece.angle == 0 and not self.speed_bump and self.pos.piece == self.lastpos.piece and self.throttle == 1:
                self.acc_k = (self.speed - self.last_speed + self.drag_k) / (self.throttle * time_unit)
            

        if self.lastpos is not None and self.lastpos.piece == self.pos.piece:

            sp = (self.speed + self.last_speed) / 2
            ang = abs(self.pos.angle) - abs(self.lastpos.angle)
            
            # max speed from the slip
            if self.pos.angle != 0 and self.lastpos.angle == 0 and self.pos.piece.radius:
                b = r2d(sp / self.pos.piece.radius)
                b_s = b - abs(self.pos.angle)
                v_t = d2r(b_s) * self.pos.piece.radius
                f_s = (v_t**2) / self.pos.piece.radius

                print "*** f_s is %f, v %f" % (f_s, v_t)
                self.slid_k = f_s

            if ang > 0 and self.pos.piece.radius > 0:
                force = (self.mass() * (sp**2)) / self.pos.piece.radius
                #print "%f angle increase on speed %f, radius %f force %f. %f a/f %f a/s %f ar/s" % (ang, sp, self.pos.piece.radius, force, ang/force, (ang/sp), (ang*self.pos.piece.radius)/sp)


    def bk_at(self, sp):
        
        # tune
        k = self.drag_k
            
        return k

        diff = sp-4.0
        if diff < 0:
            diff = 0
        return self.k * (1+((2*diff)/10))

    def brake_distance(self, tospeed, fromspeed):
        
        bk = self.bk_at(fromspeed)
        return (fromspeed - tospeed) / bk

    def brake_speed(self, length, to_speed = 0):
        return self.bk_at(to_speed) * length

    def mass(self):
        return self.width
       
    def init(self, dim, track):
        self.width = dim['width']
        self.length = dim['length']
        self.flagpos = dim['guideFlagPosition']
        self.track = track


    def dist_speed_change(self):
        # return the distance to, and the speed difference from current
        
        tp = self.pos.piece
        dist = tp.len(self.pos.offset) - self.pos.dist

        np = tp.next
        while np.max_speed(self) == tp.max_speed(self):
            dist += np.len(self.pos.offset)
            np = np.next

        new_speed = np.max_speed(self)
        return (new_speed - self.speed, dist, new_speed, self.speed)

    def throttle_to(self, speed):

        # tune

        diff = speed - self.speed

        # if within 10 %
        if self.speed == 0:
            diffp = 0.999999999
        else:
            diffp = diff / self.speed
        if diffp == 0:
            diffp = 0.000000001

        # this is .. probably stupid
        #if abs(diffp) < 0.01:
        #    ret = (self.speed/9) / (1.0 - diffp)
        if diffp < -0.10 or diff < -0.05:
            ret = 0
        elif diffp < 0:
            # decrease
            # what to maintain
            ret = (self.speed/12) / (1.0 - diffp)
            # to that .. the percentage to decrease
            ret -= (ret * 10 * abs(diffp))
        elif diffp > 0.05 or diff > self.acc_k:
            ret = 1
        else:
            ret = (self.speed/8) / (1.0 - diffp)
            ret += (ret * 10 * abs(diffp))

        ret = abs(ret)
        if ret > 1.0:
            ret = 1.0
        
        if self.speed != 0:
            log(" --  diffp %f (from %f to %f, angle %f) ret %f" % (diffp, self.speed, speed, self.angle, ret), False)
        return ret

    def update(self, data, track):

        self.angle = float(data['angle'])
        if self.max_angle < self.angle:
            self.max_angle = self.angle

        p = data['piecePosition']
        self.lastpos = self.pos
        self.pos = CarPos(p, track)
        self.pos.angle = self.angle

        # reset switch if we are switching
        if self.pos.lane != self.pos.laneto:
            self.swithingto = -1;

        tp = self.pos.piece
        if self.lastpos is not None:

            lp = self.lastpos.piece

            # calc the distance ..
            dist = 0
            pp = self.lastpos.dist
            while lp.id != tp.id:

                llo = lp.len(self.lastpos.offset)
                if llo < pp:
                    print "piec# %d we're too ahead %f vs %f" % (lp.id, llo, pp)
                    print "piece len: %d, angle %f, rad %f, offset %f" % (lp.length, lp.angle, lp.radius, self.lastpos.offset)

                dist += (llo - pp)
                lp = lp.next
                pp = 0

            #if self.pos.dist < pp:
            #    print "piece %d we're too ahead %f vs %f" % (self.pos.piece.id, self.pos.dist, pp)
            dist += self.pos.dist - pp
            self.last_speed = self.speed
            self.speed = dist / time_unit
            
            if abs(self.speed - self.last_speed) > 2:
                print "speed bump from %d:%d to %d:%d: %f => %f" % (self.lastpos.piece.id, self.lastpos.dist, self.pos.piece.id, self.pos.dist, self.last_speed, self.speed)
                # ignore it!
                #self.speed = self.last_speed
                self.speed_bump = True
            else:
                self.speed_bump = False

            if tp.radius != 0:
                self.force = (self.mass() * (self.speed**2)) / tp.radius
                if self.force > self.max_force:
                    self.max_force = self.force
            else:
                self.force = 0
                

def cid(data):
    return data['color'] + "_" + data['name']

class Race:

    def __init__(self, comm, race, mycar):

        self.comm = comm
        self.track = Track(race['track'])
        self.mycar = mycar
        self.cars = {}
        for c in race['cars']:
            car = None
            if cid(c['id']) == mycar.id:
                car = mycar
            else:
                car = Car(c['id'])

            car.init(c['dimensions'], self.track)
            self.cars[cid(c['id'])] = car

        s = race['raceSession']
        if s.has_key("durationMs"):
            self.duration = s['durationMs']
            self.laps = 0
            self.max_time = 0
            self.quick = False
        else:
            self.duration = 0
            self.laps = s['laps']
            self.max_time = s['maxLapTimeMs']
            self.quick = s['quickRace']
        self.calc_routes()

        for p in self.track.pieces:
            print "  piece %d max: %f, angle %f" % (p.id, p.max_speed(self.mycar), p.angle)
    
    def calc_routes(self):

        """
        we want an array routes[(piece, lane)] = [nextlane, total length]
        """

        self.routes = {}
        for piece in self.track.pieces:
            for lid in self.track.lanes.keys():
                lane = self.track.lanes[lid]
                l = self.remaining_routes(piece, lane, self.routes, len(self.track.pieces))
            piece = piece.next

    def remaining_routes(self, piece, lane, routes, ttl):
        
        if ttl < 0:
            return 0.0

        length = 0.0
        k = (piece.id, lane.index)
        if routes.has_key(k):
            return routes[k][1]

        if piece.switch:

            newlane = None
            i = lane.index-1
            while i < (lane.index+2):

                nl = self.track.lanes.get(i)
                i += 1
                if nl is None:
                    continue
                
                tl = piece.len(lane.offset) + self.remaining_routes(piece.next, nl, routes, ttl-1)

                if tl < length or newlane is None or (tl == length and nl == lane):
                    length = tl
                    newlane = nl

            routes[k] = (newlane.index, length)
        else:
            length = piece.len(lane.offset) + self.remaining_routes(piece.next, lane, routes, ttl-1)
            routes[k] = (lane.index, length)
        return length

    def update_positions(self, data):

        for cp in data:
            car = self.cars[cid(cp['id'])]
            car.update(cp, self.track)

    def fastest_path(self, car):
        
        try:
            (lid, length) = self.routes[(car.pos.piece.id, car.pos.lane)]
        except Exception, ex:
            print "EXXXXXX"
            for k in self.routes.keys():
                print "key %s" % str(k)
            raise ex

        # peek ahead to see if we should switch
        if car.pos.lane == lid:
            (lid2, length2) = self.routes[(car.pos.piece.next.id, lid)]
            if lid != lid2:
                lid = lid2

        speed = car.pos.piece.max_speed(car)
        (nspeed, dist, ntspeed, otspeed) = self.mycar.dist_speed_change()

        if self.last_report != car.pos.piece.id:
            print "lap %d / %d @ %d:%f pieces: %d, track %d, speed %f/%f, force %f angle %f (p: %f), canswitch %s" % (car.pos.lap, self.laps, car.pos.piece.id, car.pos.dist, car.pos.piece.id, car.pos.lane, car.speed, speed, car.force, car.angle, car.pos.piece.angle, str(car.pos.piece.switch))
            self.last_report = car.pos.piece.id

        if car.speed != 0:
            log("%f diff, next speed: %f (%f now %f/%f), dist %f" % (car.speed-speed, nspeed, ntspeed, car.speed, speed, dist), False)

        if nspeed < 0 and car.brake_distance(ntspeed, otspeed) >= dist:
            speed = 0
        return (lid, speed)

    last_report = -1

    def next_move(self):

        self.mycar.check_constants()

        (lane, speed) = self.fastest_path(self.mycar)

        # todo: we might want to switch late
        c = self.comm

        # peek ahead if we should switch

        # if not switching, and not issued yet, switch
        if self.mycar.pos.lane == self.mycar.pos.laneto and self.mycar.pos.lane != lane and self.mycar.switching != lane:
            self.mycar.switching = lane
            print "switching lanes!! %d to %d" % (self.mycar.pos.lane, lane)
            c.switchLane(self.mycar.pos.lane > lane)
        else:
            useturbo = True
            #if self.quickrace:
            #    useturbo = (self.mycar.pos.lap == (self.laps-1))
            thr = self.mycar.throttle_to(speed)
            #if abs(self.mycar.angle) >= (self.mycar.max_angle*0.95):
            #    thr = 0
            #else:
            #    thr = 1

            self.mycar.throttle = thr
            
            if thr == 1.0 and useturbo and self.mycar.turbo is not None:

                turbo = self.mycar.turbo
                # we need to make sure we have enough ehead of us!
            
                # with turbo-speed we will get this far:
                turbospeed = (self.mycar.speed + turbo.factor)
                dest = turbo.ticks * turbospeed * time_unit
                p = self.mycar.pos.piece
                d = self.mycar.pos.dist
                pc = 0
                while dest > 0:
                    pc += 1
                    if p.max_speed(self.mycar) < turbospeed:
                        turbo = None
                    d += dest
                    plen = p.len(self.mycar.pos.offset)
                    if d > plen:
                        dest = d - plen
                        p = p.next
                        d = 0
                    else:
                        dest = 0

                if turbo is not None:
                    self.mycar.turbo = None
                    print ""
                    print "************************ turbo!!!"
                    c.turbo();
                else:
                    c.throttle(thr)
            elif thr > -1 and not self.mycar.speed_bump:
                c.throttle(thr)
            else:
                c.ping()
        log(None)

    def finish(self, data):
        print "max values. Angle: %f, force: %f" % (self.mycar.max_angle, self.mycar.max_force)

    def start(self, data):
        pass

    def crash(self, data):
        self.cars[cid(data)].speed = 0

    def spawn(self, data):
        self.cars[cid(data)].speed = 0

    def lap_finished(self, data):
        pass

    def turbo_available(self, data):
        self.mycar.turbo = Turbo(data)

class NoobBot(object):


    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

        self.car = None
        self.track = None

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})
    
    def track_data(self, track, cars = 1, passwd = "randomnonsense"):

        data = { "botId" :
                     {"name": self.name,
                      "key": self.key},
                 "password": passwd,
                 "carCount": cars }
        if track != "" and track != "finland":
            data["trackName"] = track
        return data
    
    def create_race(self, track, cars = 1, passwd = "randomnonsense"):

        return self.msg("createRace", self.track_data(track, cars, passwd))

    def join_race(self, track, cars = 1, passwd = "randomnonsense"):

        return self.msg("joinRace", self.track_data(track, cars, passwd))

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def turbo(self):
        self.msg("turbo", "wrooom..")

    def switchLane(self, left):
        if left:
            left = "Left"
        else:
            left = "Right"
        self.msg("switchLane", left)

    def ping(self):
        self.msg("ping", {})

    def run(self, track = None, cars = 1, join = False):

        print "starting at %s with %d" % (track, cars)

        if track is None:
            self.join()
        elif not join:
            self.create_race(track, cars)
        else:
            self.join_race(track, cars)
        self.msg_loop()

        

    # callbacks


    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.race.start(data)
        self.ping()

    def on_car_positions(self, data):
        self.race.update_positions(data)
        self.race.next_move()

    def on_crash(self, data):
        
        # todo
        print("Someone crashed {0}".format(data))
        self.race.crash(data)
        self.race.next_move()

    def on_game_end(self, data):

        # todo
        print("Race ended")
        self.ping()

    def on_error(self, data):

        # todo
        print("Error: {0}".format(data))
        self.ping()

    def on_yourCar(self, data):
        print("YourCar: {0}".format(data))
        self.car = Car(data);
        self.ping()

    def on_gameInit(self, data):
        print("GameInit: {0}".format(data))

        self.race = Race(self, data['race'], self.car)
        self.ping()

    def on_lapFinished(self, data):

        # todo
        print("LapFinished: {0}".format(data))
        self.race.lap_finished(data)
        self.race.next_move()

    def on_finish(self, data):
        print("Finish: {0}".format(data))
        self.race.finish(data)
        self.ping()

    def on_turboAvailable(self, data):
        print("TurboAvailable: {0}".format(data))
        self.race.turbo_available(data)
        self.race.next_move()

    def on_tournamentEnd(self, data):
        print("TournamentEnd: {0}".format(data))
        self.ping()

    def on_spawn(self, data):
        print("Spawn: {0}".format(data))
        self.race.spawn(data)
        self.race.next_move()

    def on_dnf(self, data):
        print("Dnf: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,

            'yourCar': self.on_yourCar,
            'gameInit': self.on_gameInit,
            'lapFinished': self.on_lapFinished,
            'finish': self.on_finish,

            'tournamentEnd': self.on_tournamentEnd,

            'turboAvailable': self.on_turboAvailable,
            'spawn': self.on_spawn,
            'dnf': self.on_dnf,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()

            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) < 5:
        print("Usage: ./run host port botname botkey <track> <cars>")
    else:
        host, port, name, key = sys.argv[1:5]
        track, cars, join = None, 1, False
        if len(sys.argv) > 5:
            track = sys.argv[5]
        if len(sys.argv) > 6:
            cars = int(sys.argv[6])
        if len(sys.argv) > 7:
            join = True

        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run(track, cars, join)
